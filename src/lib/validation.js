const Joi = require('joi');

const headers = Joi.object().keys({
  'content-disposition': Joi.string().required(),
  'content-type': Joi.string()
    .required()
    .valid('image/jpeg', 'image/png', 'image/svg+xml', 'image/bmp', 'image/gif'),
});
const name = Joi.string().required().valid('file'); // eslint-disable-line prettier/prettier
const filename = Joi.string().required();
const byteCount = Joi.number().required();

const streamValidationSchema = Joi.object().keys({
  headers,
  name,
  filename,
  byteCount,
});

const validateStream = stream =>
  Joi.validate(stream, streamValidationSchema, { stripUnknown: true });

module.exports = {
  validateStream,
};
