const multiparty = require('multiparty');

class Multiparty {
  multipartyParse(request) {
    const form = new multiparty.Form();

    return new Promise((resolve, reject) => {
      form.on('error', error => {
        reject(error.stack);
      });

      form.on('part', part => {
        resolve(part);
      });

      form.parse(request);
    });
  }
}

module.exports = new Multiparty();
