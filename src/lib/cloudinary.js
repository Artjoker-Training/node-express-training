const cloudinary = require('cloudinary');
const config = require('../config/');

const credentials = config.cloudinary;
cloudinary.config(credentials);

class Cloudinary {
  async uploadImage(fileStream, options) {
    return new Promise((resolve, reject) => {
      const stream = cloudinary.uploader.upload_stream((result, error) => {
        if (error) {
          reject(error);
        }
        resolve(result);
      }, options);

      fileStream.pipe(stream);
    });
  }
}

module.exports = new Cloudinary();
