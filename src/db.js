const Sequelize = require('sequelize');
const config = require('./config/');

const db = {};

const { postgreSQL } = config;

const { database, userName, password, settings } = postgreSQL;

const sequelize = new Sequelize(database, userName, password, settings);

const model = sequelize.import('user', require('./api/v1/user/user-model')); // eslint-disable-line global-require, import/no-dynamic-require

db[model.name] = model;

Object.keys(db).forEach(modelName => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

(async () => {
  try {
    await sequelize.sync();
    console.log('Database sync with models'); // eslint-disable-line no-console
  } catch (err) {
    console.log(err, 'Something went wrong with the Database Update!'); // eslint-disable-line no-console
  }
})();

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
