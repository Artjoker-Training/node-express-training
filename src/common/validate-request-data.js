const Joi = require('joi');

const validateRequestData = schema => (request, response, next) => {
  request.data = Joi.validate(request.data, schema);

  if (request.data.error != null) {
    return next(request.data.error);
  }

  return next();
};

module.exports = validateRequestData;
