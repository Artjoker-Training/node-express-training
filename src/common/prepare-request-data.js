function PrepareRequestData(request, response, next) {
  request.data = {
    ...request.body,
    ...request.query,
    ...request.params,
  };

  if (request.headers.access_token) {
    request.data.accessToken = request.headers.access_token;
  }

  next();
}

module.exports = PrepareRequestData;
