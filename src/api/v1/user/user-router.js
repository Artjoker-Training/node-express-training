const express = require('express');

require('./user-config/');
const { authorization, registration, uploadAvatart } = require('./user-controller');
const {
  authorizationSchema,
  registrationSchema,
  uploadAvatarSchema,
} = require('./user-validation-schemas');
const validateRequestData = require('../../../common/validate-request-data');

const router = express.Router();

router.post('/registration', validateRequestData(registrationSchema), registration);

router.post('/authorization', validateRequestData(authorizationSchema), authorization);

router.put('/avatar', validateRequestData(uploadAvatarSchema), uploadAvatart);

module.exports = router;
