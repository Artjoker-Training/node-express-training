const STATUSES = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
};

module.exports = STATUSES;
