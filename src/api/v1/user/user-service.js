const passport = require('passport');
const { omit } = require('ramda');

const model = require('../../../db');
const { encrypt } = require('./utils/password-utils');
const { sign, verify } = require('./utils/jwt-utils');
const { uploadImage } = require('../../../lib/cloudinary');
const { cloudinaryAvatarOptions } = require('./user-config/');

class UserServices {
  generateAccessToken(user, jwtSecret) {
    return sign(user, jwtSecret);
  }

  verifyAccessToken(accessToken, jwtSecret) {
    return verify(accessToken, jwtSecret);
  }

  authorization(request) {
    return new Promise((resolve, reject) => {
      passport.authenticate('local-login', { session: false }, (error, res) => {
        if (error) {
          reject(error);
        }
        resolve(res);
      })(request);
    });
  }

  async registration(value) {
    const user = await model.user.findOne({ where: { email: value.email } });

    if (user) {
      throw new Error('This email address is already taken.');
    }

    const encryptedPassword = encrypt(value.password);

    const data = {
      ...value,
      password: encryptedPassword,
    };

    const newUser = await model.user.create(data);

    let newUserInfo = newUser.get();

    newUserInfo = omit(['password'], newUserInfo);

    return newUserInfo;
  }

  async updateAvatar(value, streamFile) {
    const user = await model.user.findOne({ where: { id: value.id } });

    if (user.avatar_byte_count === streamFile.byteCount) {
      throw new Error('Avatar cannot be updated, because avatar was not change');
    }

    const uploadedImage = await uploadImage(streamFile, cloudinaryAvatarOptions);

    let updatedUser = await user.update({
      avatar_byte_count: streamFile.byteCount,
      avatar_url: uploadedImage.url,
    });

    updatedUser = omit(['password'], updatedUser.dataValues);

    return {
      uploadedImage,
      updatedUser,
    };
  }
}

module.exports = new UserServices();
