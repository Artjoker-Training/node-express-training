const config = require('../../../config/');
const service = require('./user-service');

const { multipartyParse } = require('../../../lib/multiparty');
const { validateStream } = require('../../../lib/validation');

const { jwtSecret } = config;

class UserController {
  async authorization(request, response) {
    try {
      const user = await service.authorization(request);

      const accessToken = await service.generateAccessToken(user, jwtSecret);

      return response.status(200).json({ user, accessToken });
    } catch (error) {
      return response.status(400).json(error.message);
    }
  }

  async registration(request, response) {
    try {
      const user = await service.registration(request.data.value);
      const accessToken = service.generateAccessToken(user, jwtSecret);

      return response.status(201).json({ user, accessToken });
    } catch (error) {
      return response.status(400).json(error.message);
    }
  }

  async uploadAvatart(request, response) {
    try {
      const streamFile = await multipartyParse(request);

      const validationStreamFile = validateStream(streamFile);

      if (validationStreamFile.error != null) {
        throw new Error('Error during validation of stream files');
      }
      const userData = await service.verifyAccessToken(request.data.value.accessToken, jwtSecret);

      const responseObject = await service.updateAvatar(userData, streamFile);

      return response.status(201).json(responseObject);
    } catch (error) {
      return response.status(400).json(error.message);
    }
  }
}

module.exports = new UserController();
