const bCrypt = require('bcryptjs');

class Password {
  encrypt(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
  }

  validate(userPassword, passwordHash) {
    return bCrypt.compareSync(passwordHash, userPassword);
  }
}

module.exports = new Password();
