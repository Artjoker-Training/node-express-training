const jwt = require('jsonwebtoken');

class JWT {
  sign(user, jwtSecret) {
    return jwt.sign(user, jwtSecret);
  }

  verify(user, jwtSecret) {
    return jwt.verify(user, jwtSecret);
  }
}

module.exports = new JWT();
