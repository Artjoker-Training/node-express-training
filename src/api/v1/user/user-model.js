const USER_STATUSES = require('./constants/constants-user-model');

const userModel = (sequelize, DataTypes) =>
  sequelize.define('user', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },

    username: {
      type: DataTypes.TEXT,
    },

    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
      },
    },

    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },

    avatar_byte_count: {
      type: DataTypes.INTEGER,
    },

    avatar_url: {
      type: DataTypes.STRING,
    },

    status: {
      type: DataTypes.ENUM(Object.values(USER_STATUSES)),
      defaultValue: USER_STATUSES.ACTIVE,
    },
  });

module.exports = userModel;
