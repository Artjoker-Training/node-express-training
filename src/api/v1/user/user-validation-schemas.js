const Joi = require('joi');

const email = Joi.string()
  .required()
  .regex(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    'email',
  )
  .lowercase();
const password = Joi.string().required();
const username = Joi.string().required();
const accessToken = Joi.string().required();

const authorizationSchema = Joi.object().keys({
  email,
  password,
});

const registrationSchema = Joi.object().keys({
  username,
  email,
  password,
});

const uploadAvatarSchema = Joi.object().keys({
  accessToken,
});

module.exports = {
  authorizationSchema,
  registrationSchema,
  uploadAvatarSchema,
};
