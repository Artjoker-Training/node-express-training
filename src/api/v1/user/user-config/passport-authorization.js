const passport = require('passport');
const passportLocal = require('passport-local');
const { omit } = require('ramda');
const model = require('../../../../db');
const { validate } = require('../utils/password-utils');

const LocalStrategy = passportLocal.Strategy;

const strategyConfig = {
  usernameField: 'email',
  passwordField: 'password',
};

const login = async (email, password, done) => {
  const lowercaseEmail = email.toLowerCase();

  const user = await model.user.findOne({ where: { email: lowercaseEmail } });

  if (!user) {
    return done(new Error('Incorrect email'));
  }

  const isValidPassword = await validate(user.password, password);

  if (!isValidPassword) {
    return done(new Error('Incorrect password'));
  }

  let userInfo = await user.get();
  userInfo = omit(['password'], userInfo);

  return done(null, userInfo);
};

passport.use('local-login', new LocalStrategy(strategyConfig, login));
