const avatarOptions = {
  folder: 'avatars',
  use_filename: true,
};

module.exports = avatarOptions;
