const authorization = require('./passport-authorization');
const cloudinaryAvatarOptions = require('./cloudinary-avatar-options');

module.exports = {
  authorization,
  cloudinaryAvatarOptions,
};
