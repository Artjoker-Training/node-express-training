const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config/');
const routerApi = require('./api/');

require('./db');
const prepareData = require('./common/prepare-request-data');

const app = express();

const { port } = config;

app.use(bodyParser.json());
app.use(prepareData);
app.use('/api', routerApi);

app.listen(port, err => {
  if (err) {
    // eslint-disable-next-line no-console
    console.error(err);
  }
  // eslint-disable-next-line no-console
  console.log(`> listening on port ${port}`);
});
