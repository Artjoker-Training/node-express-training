const postgresConfig = {
  database: process.env.POSTGRE_SQL_DATABASE,
  userName: process.env.POSTGRE_SQL_USER_NAME,
  password: process.env.POSTGRE_SQL_PASSWORD,
  settings: {
    host: 'localhost',
    dialect: 'postgres',
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
};

module.exports = postgresConfig;
