require('dotenv').config();
const cloudinaryConfyg = require('./cloudinary-config');
const postgresConfig = require('./postgres-config');

const index = {
  development: {
    port: process.env.PORT,

    postgreSQL: postgresConfig,

    jwtSecret: process.env.JWT_SECRET,

    cloudinary: cloudinaryConfyg,
  },
};

module.exports = index[process.env.NODE_ENV || 'development'];
